# Kubernetes Playbook

## Setting up a new cluster

`ansible-playbook [options] cluster.yml`

### Global Variables

> `group_vars/all`

- Proxy:
  - `proxy_host`/`proxy_port`: Will be used during yum operations and Docker settings.
- API Authentication:
  - `api_user` `api_uid` `api_password` `api_token`: Will be stored into API Server's configuration files.
- `yaml_root`: path ot store yamls.
- `local_cert_path`: Kubernetes all ca files will stored here.

### Role Variables

- Docker
  - datadev、metadatadev: If datadev has value, docker will use it with old lvm style.
  - thinpooldev: if there is no value for datadev and thinpooldev is assined, docker will use thinpool instead.
  - if there both of them is empty, then Docker daemon will use loopfile.
  - logsize: limit container's disk size.

### Inventory Groups

- remote: All machines in the cluster.
- master
- node
- etcd: One or more etcd server.
- dns: if we choose kube-dns, it will deployed here.
- registry: Image registry.
- proxy: A docker server which can access hub.docker or other registrys outside the local cluster.
- newnode: If there is new servers will be the new node. We can add hosts in this section.

## Adding Node

`ansible-playbook [options] add-node.yml`

## Utilities

### `tools/fetch.image/get.image.sh`

`Usage: tools/fetch.image/get.image.sh [host file]  [origin tag] [file name]`
`tools/fetch.image/get.image.sh ubuntu:latest ubtunt.16.04`

It will fetch images through the docker server stored in "proxy" section. And save it to current path in `*.tar.gz` format.

### `tools/upgrade.sh`

`Usage: tools/upgrade.sh [host file]  [host group name] [task list]`
`Example: tools/upgrade.sh hosts.pd nodes "[etcd,flanneld]"`

Upgrade executable files and restart the services.

You can define services can be updated.

### `tools/extract_exe.sh`

`Usage: tools/extract_exe.sh [official release.tar.gz]  [target path]`
`Example: tools/extract_exe.sh kubernetes.tar.gz .`

Get executeable files from kubernetes.tar.gz

### `tools/deploy.sh`

`Usage: tools/deploy.sh [host file] [package list]`
`Example: tools/deploy.sh hosts.list "['pause','os']"`

Push image to the registry, and fill the template yaml files.

### `tools/check_service.sh`

`Usage: tools/check_service.sh [host file]`

Check services on the master and nodes.
