#!/bin/sh
set -xe
docker load -i $1
docker tag $2 $3
docker push $3
docker rmi $2
