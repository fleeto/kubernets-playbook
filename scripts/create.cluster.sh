#!/bin/sh
ansible-playbook -i hosts.fo -uroot etcd.yml
ansible-playbook -i hosts.fo -uroot master.yml
ansible-playbook -i hosts.fo -uroot node.yml
ansible-playbook -i hosts.fo -uroot registry.yml
ansible-playbook -i hosts.fo -uroot dns.yml
