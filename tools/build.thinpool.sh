#!/bin/sh

if [ $# != 1 ]
then
  echo "Usage: $0 [device name]"
  echo "Example: $0 /dev/xvdf"
  exit 1
fi

pvcreate $1
vgcreate docker $1
lvcreate --wipesignatures y -n thinpool docker -l 95%VG
lvcreate --wipesignatures y -n thinpoolmeta docker -l 1%VG
lvconvert -y --zero n -c 512K --thinpool docker/thinpool --poolmetadata docker/thinpoolmeta

PROFILE="/etc/lvm/profile/docker-thinpool.profile "
mkdir -p /etc/lvm/profile/
touch $PROFILE

tee $PROFILE << 'EOF'
activation {
  thin_pool_autoextend_threshold=80
  thin_pool_autoextend_percent=20
}
EOF


lvchange --metadataprofile docker-thinpool docker/thinpool
lvs -o+seg_monitor
