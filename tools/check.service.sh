#!/bin/sh
echo "Checking Kubelete......"
ansible -u root -i $1 -a 'systemctl is-active kubelet' node
echo "Checking Kube-proxy......"
ansible -u root -i $1 -a 'systemctl is-active kube-proxy' node
echo "Checking docker......"
ansible -u root -i $1 -a 'systemctl is-active docker' node
echo "Checking Master"
ansible -u root -i $1 -a 'systemctl is-active docker' master
ansible -u root -i $1 -a 'systemctl is-active kube-apiserver' master
ansible -u root -i $1 -a 'systemctl is-active kube-controller-manager' master
ansible -u root -i $1 -a 'systemctl is-active kube-scheduler' master
