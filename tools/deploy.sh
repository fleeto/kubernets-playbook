#!/bin/sh
if [ $# != 2 ]
then
  echo "Usage: $0 [host file] [package list]"
  echo "Example: $0 hosts.list \"['pause','os']\""
  exit 1
fi

cp scripts/deploy.yml .
ansible-playbook -i $1 -uroot deploy.yml \
-e package_list="$2"
rm -rf deploy.yml
