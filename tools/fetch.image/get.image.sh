#!/bin/sh
if [ $# != 3 ]
then
  echo "Usage: $0 [host file]  [origin tag] [new tag]"
  echo "Example: $0 ubuntu:latest ubtunt.16.04"
  exit 1
fi

HOST=$1
OLD=$2
NEW=$3
STORAGE="/tmp"

ansible proxy -i "$HOST" -u root -m copy -a "src=tools/fetch.image/pull.image dest=/usr/local/bin/pull.image.sh mode=0755"
ansible proxy -i "$HOST" -u root -a "/usr/local/bin/pull.image.sh $OLD $NEW.tar"
ansible proxy -i "$HOST" -u root -m fetch -a "src=/tmp/$NEW.tar.gz dest=$STORAGE/$NEW.tar.gz flat=yes"
ansible proxy -i "$HOST" -u root -m file -a "path=/tmp/$NEW.tar.gz state=absent"
