#!/bin/sh
if [ $# != 2 ]
then
  echo "Usage: $0 [version]  [target path]"
  echo "Example: $0 1.5.1 ."
  exit 1
fi

set -xe

URL="https://storage.googleapis.com/kubernetes-release/release/v$1/"
FILE=kubernetes-server-linux-amd64.tar.gz
rm -Rf  "/tmp/${FILE}"
rm -Rf /tmp/kubernetes
aria2c -c -s10 "${URL}${FILE}" --dir="/tmp"

SERVER="/tmp/kubernetes/server/"
DEST="$2/roles"
tar xf "/tmp/${FILE}" --directory /tmp
cp $SERVER/bin/kube-apiserver $DEST/master/files
cp $SERVER/bin/kube-controller-manager $DEST/master/files
cp $SERVER/bin/kube-scheduler $DEST/master/files
cp $SERVER/bin/kubectl $DEST/master/files
cp $SERVER/bin/kube-proxy $DEST/proxy/files
cp $SERVER/bin/kubelet $DEST/kubelet/files
cp $SERVER/bin/kube-dns $DEST/kube-dns/files
