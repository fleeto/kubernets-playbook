#!/usr/bin/python

import os
import subprocess
import re
import sys
import json

HOST = "10.211.55.37"
PORT = "8080"
KUBECTL = "/usr/local/bin/kubectl"

URL_TEMPLATE = "http://{}:{}".format(HOST, PORT)
URL_TEMPLATE = URL_TEMPLATE + \
    "/api/v1/proxy/namespaces/kube-system/services/heapster" + \
    "/api/v1/model/namespaces/{}/pods/{}/metrics/{}"

def get_rc_pods(namespace, obj_name):
    command = [KUBECTL, "-s", "http://{}:{}".format(HOST,PORT),  "get", "rc", obj_name,
               "--namespace=" + namespace, "-o", "json"]
    json_str = subprocess.check_output(command)
    json_obj = json.loads(json_str)
    selector_list = json_obj["spec"]["selector"]
    pod_list_json = get_pods_by_label(namespace, selector_list)
    pod_list = json.loads(pod_list_json)["items"]
    info_list = []
    for pod in pod_list:
        if (pod["kind"] == "Pod") and (pod["status"]["phase"] == "Running"):
            info_list = info_list + [pod["metadata"]["name"]]
    return info_list

def get_deploy_pods(namespace, obj_name):
    command = [KUBECTL, "-s", "http://{}:{}".format(HOST,PORT),  "get", "rc", obj_name,
               "--namespace=" + namespace, "-o", "json"]
    json_str = subprocess.check_output(command)
    json_obj = json.loads(json_str)
    selector_list = json_obj["spec"]["selector"]["matchLabels"]
    pod_list_json = get_pods_by_label(namespace, selector_list)
    pod_list = json.loads(pod_list_json)["items"]
    info_list = []
    for pod in pod_list:
        if (pod["kind"] == "Pod") and (pod["status"]["phase"] == "Running"):
            info_list = info_list + [pod["metadata"]["name"]]
    return info_list

def get_pods_by_label(namespace, selector_list):
    command = [KUBECTL, "-s", "http://{}:{}".format(HOST,PORT),  "get", "pods", "--namespace=" + namespace]
    for key in selector_list:
        value = selector_list[key]
        command = command + ["-l", key + "=" + value]
    command = command + ["-o", "json"]
    return subprocess.check_output(command)

def get_pods(namespace, obj_type, obj_name):
    command = [KUBECTL, "-s", "http://{}:{}".format(HOST,PORT),  "get", obj_type, obj_name,
               "--namespace=" + namespace, "-o", "json"]
    json_str = subprocess.check_output(command)
    json_obj = json.loads(json_str)
    if (obj_type == "deploy"):
        selector_list = json_obj["spec"]["selector"]["matchLabels"]
    if (obj_type == "rc"):
        selector_list = json_obj["spec"]["selector"]

    pod_list_json = get_pods_by_label(namespace, selector_list)
    pod_list = json.loads(pod_list_json)["items"]
    info_list = []
    for pod in pod_list:
        if (pod["kind"] == "Pod") and (pod["status"]["phase"] == "Running"):
            info_list = info_list + [pod["metadata"]["name"]]
    return info_list

def get_last_data(json_str):
    obj = json.loads(json_str)
    last_key = obj["latestTimestamp"]
    for item in obj["metrics"]:
        if (item["timestamp"] == last_key):
            return item["value"]
    return 0

def fetch_data(namespace, obj_type, obj_name, metric):
    pod_name_list = get_pods(namespace, obj_type, obj_name)
    if (metric == "count"):
        return len(pod_name_list)
    if (metric == "metric"):
        url = URL_TEMPLATE.format(namespace, pod_name_list[0], "")
        return subprocess.check_output(["curl", "-s", "-q", url])
    total = 0
    for pod_name in pod_name_list:
        url = URL_TEMPLATE.format(namespace, pod_name, metric)
        json_str = subprocess.check_output(["curl", "-s", "-q", url])
        total = total + get_last_data(json_str)
    return total

if __name__ == "__main__":
    if (len(sys.argv) == 5):
        print fetch_data(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        print "Usage %s [namespace] [rc/deploy] [name] [metric]" % sys.argv[0]
