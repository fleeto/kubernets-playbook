#!/bin/sh
VERSION=$1
pushd
cd /tmp
wget -c "https://dl.k8s.io/v$VERSION/kubernetes-server-linux-amd64.tar.gz"
tar xf kubernetes-server-linux-amd64.tar.gz
popd
cp /tmp/kubernetes/server/bin/kube-apiserver files/kubernetes/
cp /tmp/kubernetes/server/bin/kube-controller-manager files/kubernetes/
cp /tmp/kubernetes/server/bin/kube-proxy files/kubernetes/
cp /tmp/kubernetes/server/bin/kubectl files/kubernetes/
cp /tmp/kubernetes/server/bin/kubelet files/kubernetes/
rm -Rf /tmp/kubernetes