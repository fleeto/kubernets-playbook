#!/bin/sh
if [ $# != 3 ]
then
  echo "Usage: $0 [host file]  [host group name] [task list]"
  echo "Example: $0 hosts.pd nodes \"[etcd,flanneld]\""
  exit 1
fi

cp scripts/upgrade.yml .

ansible-playbook -i \
 $1 -uroot \
 -e host_list=$2 \
 -e task_list=$3 \
 upgrade.yml

rm -rf upgrade.yml
