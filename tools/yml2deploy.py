#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import subprocess
import os.path
import requests
import re
from datetime import datetime


def create_info_file(path, image_list):
    file_handle = open(os.path.join(path, "info.yml"), "w")
    file_handle.write("---\nimages:\n")
    for image in image_list:
        file_handle.write("  - file: {}\n".format(image["file"]))
        file_handle.write("    dest: {}\n".format(image["dest"]))
        file_handle.write("    src: {}\n".format(image["src"]))
    file_handle.write("templates:\n")
    file_handle.write("  - single.yaml\n")
    file_handle.write("registry_prefix: \"{{ groups['registry'][0] }}:{{ registry_port }}/\"\n")
    file_handle.close()


def fetch_image(image_remote, host_file, image_file_name, target_dir):
    fetch_command = ["tools/fetch.image/get.image.sh", host_file, image_remote, image_file_name]
    mv_command = ["mv", image_file_name+".tar.gz", target_dir]
    subprocess.check_call(fetch_command)
    subprocess.check_call(mv_command)


def main(yaml, host_file, target_dir):
    if not os.path.isfile(host_file):
        print "invalid host file, existing.\n"
        exit(2)
    if not os.path.isdir(target_dir):
        print "invalid target dir, existing.\n"
        exit(2)

    if yaml[:4] == "http":
        result = requests.get(yaml)
        content = result.content
        lines = content.split("\n")
    else:
        if not os.path.isfile(yaml):
            print "invalid yaml file, existing. \n"
            exit(2)
        else:
            handler = open(yaml)
            lines = []
            for item in handler:
                lines.append(item)

    yaml_file_name = "single.yaml"
    yaml_handler = open(os.path.join(target_dir, yaml_file_name), "w")

    regex = re.compile("^([\s-]+image:\s+)(.*?)$")
    subst = "\\1{{ registry_prefix }}\\2"

    image_list = []
    for line in lines:
        mat = regex.match(line)
        if not mat:
            yaml_handler.write(line)
        else:
            image_name_tag = mat.group(2)
            tar_name = image_name_tag.replace(":", ".")
            tar_name = tar_name.replace("/", ".")
            fetch_image(image_name_tag, host_file, tar_name, target_dir)
            image_record = {
                "src": image_name_tag,
                "file": tar_name + ".tar.gz",
                "dest": image_name_tag
            }
            image_list.append(image_record)
            yaml_handler.write(regex.sub(subst, line))
    yaml_handler.close()

    create_info_file(target_dir, image_list)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print "Usage: {} [yaml] [host_file] [target dir]\n".format(sys.argv[0])
        exit(1)
    main(sys.argv[1], sys.argv[2], sys.argv[3])
